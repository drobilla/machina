#!/usr/bin/env python

import os
import subprocess

from waflib.extras import autowaf

# Version of this package (even if built as a child)
MACHINA_VERSION = '0.5.0'

# Mandatory waf variables
APPNAME = 'machina'
VERSION = MACHINA_VERSION
top     = '.'
out     = 'build'

def options(opt):
    opt.load('compiler_cxx')

def configure(conf):
    conf.load('compiler_cxx', cache=True)
    conf.load('autowaf', cache=True)
    autowaf.set_cxx_lang(conf, 'c++17')

    conf.check_pkg('lv2 >= 1.2.0', uselib_store='LV2')
    conf.check_pkg('glibmm-2.4 >= 2.14.0', system=True, uselib_store='GLIBMM')
    conf.check_pkg('gthread-2.0 >= 2.14.0', system=True, uselib_store='GTHREAD')
    conf.check_pkg('gtkmm-2.4 >= 2.12.0', system=True, uselib_store='GTKMM', mandatory=False)
    conf.check_pkg('jack >= 0.120.0', uselib_store='JACK')
    conf.check_pkg('raul-1 >= 1.1.0', uselib_store='RAUL')
    conf.check_pkg('ganv-1 >= 1.2.1', uselib_store='GANV', mandatory=False)
    conf.check_pkg('serd-0 >= 0.4.0', uselib_store='SERD', mandatory=False)
    conf.check_pkg('sord-0 >= 0.4.0', uselib_store='SORD', mandatory=False)
    conf.check_pkg('eugene', uselib_store='EUGENE', mandatory=False)

    # Check for posix_memalign (OSX, amazingly, doesn't have it)
    conf.check_function('cxx', 'posix_memalign',
                        header_name = 'stdlib.h',
                        define_name = 'HAVE_POSIX_MEMALIGN',
                        return_type = 'int',
                        arg_types   = 'void**,size_t,size_t',
                        mandatory   = False)

    if conf.env.HAVE_GTKMM and conf.env.HAVE_GANV:
        conf.env.MACHINA_BUILD_GUI = True

    conf.define('MACHINA_PPQN', 19200)
    conf.define('MACHINA_DATA_DIR', os.path.join(conf.env.DATADIR, 'machina'))

    conf.write_config_header('machina_config.h', remove=False)

    autowaf.display_summary(conf,
                            {'Jack': bool(conf.env.HAVE_JACK),
                             'GUI': bool(conf.env.MACHINA_BUILD_GUI)})

def build(bld):
    bld.recurse('src/engine')
    bld.recurse('src/client')

    if bld.env.MACHINA_BUILD_GUI:
        bld.recurse('src/gui')

def lint(ctx):
    subprocess.call('cpplint.py --filter=-whitespace/comments,-whitespace/tab,-whitespace/braces,-whitespace/labels,-build/header_guard,-readability/casting,-readability/todo,-build/namespaces,-whitespace/line_length,-runtime/rtti,-runtime/references,-whitespace/blank_line,-runtime/sizeof,-readability/streams,-whitespace/operators,-whitespace/parens,-build/include,-build/storage_class `find -name *.cpp -or -name *.hpp`', shell=True)
